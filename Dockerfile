FROM node:12.8.0-alpine

WORKDIR app

COPY . /app

RUN npm install

CMD node app.js
